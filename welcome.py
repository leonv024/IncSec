#!/usr/bin/python
import sys, os, getpass, time, datetime, subprocess, socket, platform
from random import randint

def uptime():
    # Uptime was not written by me because I am to lazy to do math
     try:
         f = open( "/proc/uptime" )
         contents = f.read().split()
         f.close()
     except:
        return "Cannot open uptime file: /proc/uptime"

     total_seconds = float(contents[0])

     # Helper vars:
     MINUTE  = 60
     HOUR    = MINUTE * 60
     DAY     = HOUR * 24

     # Get the days, hours, etc:
     days    = int( total_seconds / DAY )
     hours   = int( ( total_seconds % DAY ) / HOUR )
     minutes = int( ( total_seconds % HOUR ) / MINUTE )
     seconds = int( total_seconds % MINUTE )

     # Build up the pretty string (like this: "N days, N hours, N minutes, N seconds")
     string = ""
     if days > 0:
         string += str(days) + " " + (days == 1 and "day" or "days" ) + ", "
     if len(string) > 0 or hours > 0:
         string += str(hours) + " " + (hours == 1 and "hour" or "hours" ) + ", "
     if len(string) > 0 or minutes > 0:
         string += str(minutes) + " " + (minutes == 1 and "minute" or "minutes" ) + ", "
     string += str(seconds) + " " + (seconds == 1 and "second" or "seconds" )

     return string

def time_time():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
def time_date():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y')


def banner_select():
    # Banners
    banner1 = ("""
 ______                  ____                       ____                     __
/\__  _\                /\  _`\                    /\  _`\                  /\ \__
\/_/\ \/     ___     ___\ \,\L\_\     __    ___    \ \,\L\_\  __  __    ____\ \ ,_\    __    ___ ___     ____
   \ \ \   /' _ `\  /'___\/_\__ \   /'__`\ /'___\   \/_\__ \ /\ \/\ \  /',__\\\\ \ \/  /'__`\/' __` __`\  /',__\\
    \_\ \__/\ \/\ \/\ \__/ /\ \L\ \/\  __//\ \__/     /\ \L\ \ \ \_\ \/\__, `\\\\ \ \_/\  __//\ \/\ \/\ \/\__, `\\
    /\_____\ \_\ \_\ \____\\\\ `\____\ \____\ \____\    \ `\____\/`____ \/\____/ \ \__\ \____\ \_\ \_\ \_\/\____/
    \/_____/\/_/\/_/\/____/ \/_____/\/____/\/____/     \/_____/`/___/> \/___/   \/__/\/____/\/_/\/_/\/_/\/___/
                                                                  /\___/
                                                                  \/__/                            """)


    banner2 = ("""
      _____             _____              _____           _
    |_   _|           / ____|            / ____|         | |
      | |  _ __   ___| (___   ___  ___  | (___  _   _ ___| |_ ___ _ __ ___  ___
      | | | '_ \ / __|\___ \ / _ \/ __|  \___ \| | | / __| __/ _ \ '_ ` _ \/ __|
     _| |_| | | | (__ ____) |  __/ (__   ____) | |_| \__ \ ||  __/ | | | | \__ \\
    |_____|_| |_|\___|_____/ \___|\___| |_____/ \__, |___/\__\___|_| |_| |_|___/
                                                 __/ |
                                                |___/""")


    banner3 = ("""
    @@@  @@@  @@@   @@@@@@@   @@@@@@   @@@@@@@@   @@@@@@@
@@@  @@@@ @@@  @@@@@@@@  @@@@@@@   @@@@@@@@  @@@@@@@@
@@!  @@!@!@@@  !@@       !@@       @@!       !@@
!@!  !@!!@!@!  !@!       !@!       !@!       !@!
!!@  @!@ !!@!  !@!       !!@@!!    @!!!:!    !@!
!!!  !@!  !!!  !!!        !!@!!!   !!!!!:    !!!
!!:  !!:  !!!  :!!            !:!  !!:       :!!
:!:  :!:  !:!  :!:           !:!   :!:       :!:
 ::   ::   ::   ::: :::  :::: ::    :: ::::   ::: :::
:    ::    :    :: :: :  :: : :    : :: ::    :: :: :


 @@@@@@   @@@ @@@   @@@@@@   @@@@@@@  @@@@@@@@  @@@@@@@@@@    @@@@@@
@@@@@@@   @@@ @@@  @@@@@@@   @@@@@@@  @@@@@@@@  @@@@@@@@@@@  @@@@@@@
!@@       @@! !@@  !@@         @@!    @@!       @@! @@! @@!  !@@
!@!       !@! @!!  !@!         !@!    !@!       !@! !@! !@!  !@!
!!@@!!     !@!@!   !!@@!!      @!!    @!!!:!    @!! !!@ @!@  !!@@!!
 !!@!!!     @!!!    !!@!!!     !!!    !!!!!:    !@!   ! !@!   !!@!!!
     !:!    !!:         !:!    !!:    !!:       !!:     !!:       !:!
    !:!     :!:        !:!     :!:    :!:       :!:     :!:      !:!
:::: ::      ::    :::: ::      ::     :: ::::  :::     ::   :::: ::
:: : :       :     :: : :       :     : :: ::    :      :    :: : :
                                                                      """)


    banner4 = ("""
    _ _  _ ____ ____ ____ ____    ____ _   _ ____ ___ ____ _  _ ____
    | |\ | |    [__  |___ |       [__   \_/  [__   |  |___ |\/| [__
    | | \| |___ ___] |___ |___    ___]   |   ___]  |  |___ |  | ___]""")


    banner5 = ("""
    '####:'##::: ##::'######:::'######::'########::'######::
    . ##:: ###:: ##:'##... ##:'##... ##: ##.....::'##... ##:
    : ##:: ####: ##: ##:::..:: ##:::..:: ##::::::: ##:::..::
    : ##:: ## ## ##: ##:::::::. ######:: ######::: ##:::::::
    : ##:: ##. ####: ##::::::::..... ##: ##...:::: ##:::::::
    : ##:: ##:. ###: ##::: ##:'##::: ##: ##::::::: ##::: ##:
    '####: ##::. ##:. ######::. ######:: ########:. ######::
    ....::..::::..:::......::::......:::........:::......:::
    :'######::'##:::'##::'######::'########:'########:'##::::'##::'######::
    '##... ##:. ##:'##::'##... ##:... ##..:: ##.....:: ###::'###:'##... ##:
     ##:::..:::. ####::: ##:::..::::: ##:::: ##::::::: ####'####: ##:::..::
    . ######::::. ##::::. ######::::: ##:::: ######::: ## ### ##:. ######::
    :..... ##:::: ##:::::..... ##:::: ##:::: ##...:::: ##. #: ##::..... ##:
    '##::: ##:::: ##::::'##::: ##:::: ##:::: ##::::::: ##:.:: ##:'##::: ##:
    . ######::::: ##::::. ######::::: ##:::: ########: ##:::: ##:. ######::
    :......::::::..::::::......::::::..:::::........::..:::::..:::......:::""")

    banner = randint(1,5)
    if banner == 1:
        return banner1
    elif banner == 2:
        return banner2
    elif banner == 3:
        return banner3
    elif banner == 4:
        return banner4
    elif banner == 5:
        return banner5

# Check diskspace usage
du = os.popen("df -h /").readlines()

# Get IP
try:
    ip = [l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0]
except socket.gaierror:
    ip = 'error'
except Exception:
    ip = 'error'

# Print final result
print("\033[1;36m%s\033[0m\n" % banner_select())
print('\033[1;33mWelcome to the system, \033[91m%s\033[0m\n' % getpass.getuser())

print('\033[1;32mDate:\033[0m %s \033[1;32mTime:\033[0m %s \033[1;32mUptime:\033[0m %s\n' % (time_date().ljust(20), time_time().ljust(20), uptime()))
print('\033[1;32mOS:\033[0m %s \033[1;32mHostname:\033[0m %s \033[1;32mIP:\033[0m %s' % (platform.system().ljust(22),socket.gethostname().ljust(16), ip))

print('\n\033[1;32m%s\033[0m%s' % (du[0].replace("'", ""), du[1].replace("'", "")))
os.system('/bin/bash')

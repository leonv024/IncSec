#!/usr/bin/python
import sys, os, getpass, time, datetime, subprocess, socket, platform
from random import randint

def uptime():
    # Uptime was not written by me because I am to lazy to do math
     try:
         f = open( "/proc/uptime" )
         contents = f.read().split()
         f.close()
     except:
        return "Cannot open uptime file: /proc/uptime"

     total_seconds = float(contents[0])

     # Helper vars:
     MINUTE  = 60
     HOUR    = MINUTE * 60
     DAY     = HOUR * 24

     # Get the days, hours, etc:
     days    = int( total_seconds / DAY )
     hours   = int( ( total_seconds % DAY ) / HOUR )
     minutes = int( ( total_seconds % HOUR ) / MINUTE )
     seconds = int( total_seconds % MINUTE )

     # Build up the pretty string (like this: "N days, N hours, N minutes, N seconds")
     string = ""
     if days > 0:
         string += str(days) + " " + (days == 1 and "day" or "days" ) + ", "
     if len(string) > 0 or hours > 0:
         string += str(hours) + " " + (hours == 1 and "hour" or "hours" ) + ", "
     if len(string) > 0 or minutes > 0:
         string += str(minutes) + " " + (minutes == 1 and "minute" or "minutes" ) + ", "
     string += str(seconds) + " " + (seconds == 1 and "second" or "seconds" )

     return string

def time_time():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
def time_date():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y')


def banner_select():
    # Banners
    banner1 = ("""\033[1;36m
 ______                  ____                       ____                     __
/\__  _\                /\  _`\                    /\  _`\                  /\ \__
\/_/\ \/     ___     ___\ \,\L\_\     __    ___    \ \,\L\_\  __  __    ____\ \ ,_\    __    ___ ___     ____
   \ \ \   /' _ `\  /'___\/_\__ \   /'__`\ /'___\   \/_\__ \ /\ \/\ \  /',__\\\\ \ \/  /'__`\/' __` __`\  /',__\\
    \_\ \__/\ \/\ \/\ \__/ /\ \L\ \/\  __//\ \__/     /\ \L\ \ \ \_\ \/\__, `\\\\ \ \_/\  __//\ \/\ \/\ \/\__, `\\
    /\_____\ \_\ \_\ \____\\\\ `\____\ \____\ \____\    \ `\____\/`____ \/\____/ \ \__\ \____\ \_\ \_\ \_\/\____/
    \/_____/\/_/\/_/\/____/ \/_____/\/____/\/____/     \/_____/`/___/> \/___/   \/__/\/____/\/_/\/_/\/_/\/___/
                                                                  /\___/
                                                                  \/__/\033[0m

    \033[1;33mWelcome to the system, \033[91m%s

    \033[1;32m[ Today is ]\033[0m %s %s \t\033[1;32m[ Uptime ]\033[0m %s

    \033[1;32m[ Hostname ]\033[0m %s \t\033[1;32m[ Connected ]\033[0m %s
    """ % (getpass.getuser(), time_date(), time_time(), uptime().ljust(15),socket.gethostname().ljust(15), ip.ljust(15)))


    banner2 = ("""\033[1;36m
      _____             _____              _____           _
    |_   _|           / ____|            / ____|         | |
      | |  _ __   ___| (___   ___  ___  | (___  _   _ ___| |_ ___ _ __ ___  ___
      | | | '_ \ / __|\___ \ / _ \/ __|  \___ \| | | / __| __/ _ \ '_ ` _ \/ __|
     _| |_| | | | (__ ____) |  __/ (__   ____) | |_| \__ \ ||  __/ | | | | \__ \\
    |_____|_| |_|\___|_____/ \___|\___| |_____/ \__, |___/\__\___|_| |_| |_|___/
                                                 __/ |
                                                |___/\033[0m

    \033[1;33mWelcome to the system, \033[91m%s

    \033[1;32m[ Today is ]\033[0m %s %s \t\033[1;32m[ Uptime ]\033[0m %s

    \033[1;32m[ Hostname ]\033[0m %s \t\033[1;32m[ Connected ]\033[0m %s
    """ % (getpass.getuser(), time_date(), time_time(), uptime().ljust(15),socket.gethostname().ljust(15), ip.ljust(15)))


    banner3 = ("""
                ..:::::::::..
           ..:::\033[1;91maad8888888baa\033[0m:::.. \033[1;33mWelcome to the system, \033[91m%s\033[90m
        .::::\033[1;91md:?88888888888?::8b\033[0m::::.
      .:::\033[1;91md8888:?88888888??a888888b\033[0m:::.
    .:::\033[1;91md8888888a8888888aa8888888888b\033[0m:::. \033[1;32m[ Today is ]\033[0m %s %s\033[1;90m
   ::::\033[1;91mdP::::::::88888888888::::::::Yb\033[0m::::
  ::::\033[1;91mdP:::::::::Y888888888P:::::::::Yb\033[0m::::
 ::::\033[1;91md8:::::::::::Y8888888P:::::::::::8b\033[0m:::: \033[1;32m[ Uptime ]\033[0m %s\033[1;90m
.::::\033[1;91m88::::::::::::Y88888P::::::::::::88\033[0m::::.
:::::\033[1;91mY8baaaaaaaaaa88P:T:Y88aaaaaaaaaad8P\033[0m:::::
:::::::\033[1;91mY88888888888P::|::Y88888888888P\033[0m::::::: \033[1;32m[ Hostname ]\033[0m %s\033[1;90m
::::::::::::::::\033[1;91m888:::|:::888\033[0m::::::::::::::::
`:::::::::::::::\033[1;91m8888888888888b\033[0m::::::::::::::'
 :::::::::::::::\033[1;91m88888888888888\033[0m:::::::::::::: \033[1;32m[ Connected ]\033[0m %s\033[1;90m
  :::::::::::::\033[1;91md88888888888888\033[0m:::::::::::::
   ::::::::::::\033[1;91m88::88::88:::88\033[0m::::::::::::
    `::::::::::\033[1;91m88::88::88:::88\033[0m::::::::::' \033[1;36mIncSec Systems\033[1;90m
      `::::::::\033[1;91m88::88::P::::88\033[0m::::::::'
        `::::::\033[1;91m88::88:::::::88\033[0m::::::'
           ``:::::::::::::::::::''
                ``:::::::::''
                """ % (getpass.getuser(), time_date(), time_time(), uptime(),socket.gethostname(), ip))


    banner4 = ("""\033[1;36m
    _ _  _ ____ ____ ____ ____    ____ _   _ ____ ___ ____ _  _ ____
    | |\ | |    [__  |___ |       [__   \_/  [__   |  |___ |\/| [__
    | | \| |___ ___] |___ |___    ___]   |   ___]  |  |___ |  | ___]\033[0m

    \033[1;33mWelcome to the system, \033[91m%s

    \033[1;32m[ Today is ]\033[0m %s %s \t\033[1;32m[ Uptime ]\033[0m %s

    \033[1;32m[ Hostname ]\033[0m %s \t\033[1;32m[ Connected ]\033[0m %s
    """ % (getpass.getuser(), time_date(), time_time(), uptime().ljust(15),socket.gethostname().ljust(15), ip.ljust(15)))


    banner5 = ("""\033[1;90m
                      _..-'(                       )`-.._
                   ./'. '||\\\\.       (\_/)       .//||` .`\. \t\t\033[1;33mWelcome to the system, \033[91m%s\033[90m
                ./'.|'.'||||\\\\|..    )\033[1;91mO O\033[1;90m(    ..|//||||`.`|.`\.\\
             ./'..|'.|| |||||\`````` '`"'` ''''''/||||| ||.`|..`\. \t\t\033[1;32m[ Today is ]\033[0m %s %s\033[1;90m
           ./'.||'.|||| ||||||||||||.     .|||||||||||| |||||.`||.`\.
          /'|||'.|||||| ||||||||||||{ Inc }|||||||||||| ||||||.`|||`\\ \t\t\033[1;32m[ Uptime ]\033[0m %s\033[1;90m
         '.|||'.||||||| ||||||||||||{ Sec }|||||||||||| |||||||.`|||.`
        '.||| ||||||||| |/'   ``\||``     ''||/''   `\| ||||||||| |||.` \t\033[1;32m[ Hostname ]\033[0m %s\033[1;90m
        |/' \./'     `\./         \!|\   /|!/         \./'     `\./ `\|
        V    V         V          }' `\ /' `{          V         V    V \t\033[1;32m[ Connected ]\033[0m %s\033[1;90m
        `    `         `               V               '         '    '\033[0m""" % (getpass.getuser(), time_date(), time_time(), uptime(),socket.gethostname(), ip))

    banner = randint(1,5)
    if banner == 1:
        return banner1
    elif banner == 2:
        return banner2
    elif banner == 3:
        return banner3
    elif banner == 4:
        return banner4
    elif banner == 5:
        return banner5



# Check diskspace usage
#du = os.popen("df -h /").readlines()

# Get IP
try:
    ip = [l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0]
except socket.gaierror:
    ip = '0.0.0.0'
except Exception:
    ip = '0.0.0.0'

# Print final result
print(banner_select())

#print('\n\033[1;32m%s\033[0m%s' % (du[0].replace("'", ""), du[1].replace("'", "")))

os.system('/bin/bash')
